#ifndef INCLUDED_TYPEDEFS_
#define INCLUDED_TYPEDEFS_

#include <vector>
#include <string>

using BoolVect = std::vector<bool>;
using BoolMatrix = std::vector<BoolVect>;
using StringVect = std::vector<std::string>;
using StringMatrix = std::vector<StringVect>;
using UnsignedVect = std::vector<unsigned>;

#endif
